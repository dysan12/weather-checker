// @ts-ignore
import React, { Component } from 'react';
import Menu from "./Components/Menu";
import Map from './Components/Map';
import History from './Components/History';
import { BrowserRouter as Router, Route, RouteComponentProps } from "react-router-dom";

export interface RouteMetadata {
    path: string,
    label: string,
    component: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>,
    exact: boolean
}

const routes: RouteMetadata[] = [
    {path: '/', label: 'Mapa', component: Map, exact: true},
    {path: '/history', label: 'Historia', component: History, exact: false},
];

class App extends Component {
  render() {
    return (
      <Router>
          <div>
              <Menu routes={routes}/>
              {routes.map(route => <Route key={route.path} path={route.path} component={route.component} exact={route.exact}/>)}
          </div>
      </Router>
    );
  }
}

export default App;
