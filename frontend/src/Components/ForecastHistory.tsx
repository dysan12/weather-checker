import * as React from 'react';
import ReactTable from "react-table";
import ReactLoader from 'react-loader';
import Fetcher from '../API/Fetcher';


interface ForecastHistoryState {
    forecasts: Forecast[]
    pagination: Pagination
    isLoading: boolean
}

interface Forecast {
    city: string,
    cloudiness: string,
    description: string,
    temperature: string,
    windSpeed: string
}

interface Pagination {
    page: number,
    limit: number,
    size: number
}

export default class ForecastHistory extends React.Component<{}, ForecastHistoryState> {
    state = {
        'forecasts': [],
        'pagination': {
            'page': 1,
            'limit': 10,
            'size': 0
        },
        'isLoading': true
    };

    async componentDidMount() {
        this.loadForecasts(this.state.pagination.page - 1)
    }

    public loadForecasts = async (page: number) => {
        page++; // strange logic required by react-table
        try {
            this.setState((prevState) => {
                return { ...prevState, isLoading: true, ...{ ...prevState.pagination, 'page': page } }
            });
            const response = await Fetcher.fetchHistoricalForecasts(this.state.pagination.limit, page);
            const body = await response.json();

            this.setState((prevState) => {
                return {
                    ...prevState, ...{
                        'forecasts': body.forecasts,
                        'pagination': {
                            'page': body._pagination.page,
                            'limit': body._pagination.limit,
                            'size': body._pagination.size
                        },
                        'isLoading': false
                    }
                }
            });
        } catch (e) {

        }
    };

    public render() {
        const { forecasts, isLoading, pagination } = this.state;
        return (
            <section className={'history-table'}>
                <ReactLoader
                    loaded={!isLoading}
                    className={"GridLoader"}
                    options={{
                        position: 'relative'
                    }}
                >
                    <ReactTable
                        data={forecasts}
                        columns={[
                            { Header: 'Miasto', accessor: 'city' },
                            { Header: 'Temperatura', accessor: 'temperature' },
                            { Header: 'Zachmurzenie', accessor: 'cloudiness' },
                            { Header: 'Prędkość wiatru', accessor: 'windSpeed' },
                            { Header: 'Opis', accessor: 'description' },
                        ]}
                        manual={true}
                        page={pagination.page - 1}
                        pages={Math.ceil(pagination.size / pagination.limit)}
                        sortable={false}
                        showPagination={true}
                        showPageSizeOptions={false}
                        minRows={0}
                        pageSize={pagination.limit}
                        onPageChange={this.loadForecasts}
                    />
                </ReactLoader>
            </section>
        );
    }
}
