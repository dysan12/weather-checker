import * as React from 'react';
import 'react-table/react-table.css'
import Statistics from "./Statistics";
import ForecastHistory from "./ForecastHistory";

export default class History extends React.Component<{}, {}> {
    public render() {
        return (
            <section className={'history-section'}>
                <Statistics/>
                <ForecastHistory/>
            </section>
        );
    }
}
