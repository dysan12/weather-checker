import * as React from 'react';
import { Link } from "react-router-dom";
import { RouteMetadata } from "../App";

interface MenuProps {
    routes: RouteMetadata[]
}

export default class Menu extends React.Component<MenuProps, {}> {
    constructor(props: MenuProps) {
        super(props);

    }

    public render() {
        return (
            <ul className={'menu'}>
                {this.props.routes.map(route => <li key={route.path}><Link to={route.path}>{route.label}</Link></li>)}
            </ul>
        );
    }
}
