import * as React from 'react';
import GoogleMap from "./GoogleMap";

interface MapProps {
}

export default class Map extends React.Component<MapProps, {}> {
    constructor(props: MapProps) {
        super(props);

    }

    public render() {
        return (
            <div>
                <GoogleMap defaultCenter={{lat: 50.25, lng: 19.0}} zoom={12} />
            </div>
        );
    }
}
