import * as React from 'react';
import ReactLoader from 'react-loader';
import Fetcher from '../API/Fetcher';

interface StatisticsState {
    statistics: {
                    maxTemp: number,
                    minTemp: number,
                    avgTemp: number
                    searchesQuantity: number,
                    mostSearchedCity: string
                } | null,
    isLoading: boolean;
}

export default class Statistics extends React.Component<{}, StatisticsState> {
    state = {
        statistics: null,
        isLoading: false
    };

    componentDidMount(): void {
        this.loadStatistics();
    }

    public loadStatistics = async () => {
        try {
            this.setState((prevState) => {
                return { ...prevState, isLoading: true }
            });
            const response = await Fetcher.fetchStatistics();
            const body = await response.json();

            this.setState((prevState) => {
                return {
                    ...prevState, ...{
                        statistics: {
                            maxTemp: body.maxTemp,
                            minTemp: body.minTemp,
                            avgTemp: body.avgTemp,
                            searchesQuantity: body.searchesQuantity,
                            mostSearchedCity: body.mostSearchedCity,
                        },
                        isLoading: false
                    }
                }
            });
        } catch (e) {
            this.setState((prevState) => {
                return { ...prevState, statistics: null, isLoading: false }
            });

        }
    };

    public render() {
        const { isLoading, statistics } = this.state;

        let stats;
        if (statistics !== null) {
            stats = (
                <div className={'statistics'}>
                    <header>
                        <h3>Statystyki wyszukiwań</h3>
                    </header>
                    <div>
                        <p><span className={'bold'}>Maksymalna temperatura: </span>{statistics.maxTemp}</p>
                        <p><span className={'bold'}>Minimalna temperatura: </span>{statistics.minTemp}</p>
                        <p><span className={'bold'}>Średnia temperatura: </span>{statistics.avgTemp}</p>
                        <p><span className={'bold'}>Łączna ilość zapytań: </span>{statistics.searchesQuantity}</p>
                        <p><span className={'bold'}>Najczęściej wyszukiwane miasto: </span>{statistics.mostSearchedCity}
                        </p>
                    </div>
                </div>
            )
        } else {
            stats = (
                <div className={'statistics'}>
                    <div>
                        <span>Nie udało się załadować danych. <span className={"link pointer"}
                                                                    onClick={this.loadStatistics}>Spróbuj ponownie.</span></span>
                    </div>
                </div>
            )
        }

        return (
            <section className={"statistics-section"}>
                <ReactLoader
                    loaded={!isLoading}
                    className={"GridLoader"}
                    options={{
                        position: 'relative'
                    }}
                >
                    {stats}
                </ReactLoader>
            </section>
        );
    }
}
