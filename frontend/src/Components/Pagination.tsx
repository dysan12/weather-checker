import * as React from 'react';

interface PaginationProps {
    page: number,
    limit: number,
    size: number,
    onChose: (page: number) => void;
}

export default function Pagination({page, limit, size, onChose}: PaginationProps) {
    const isLast = (): boolean => {
        return Math.ceil(size / limit) === page;
    };

    const isFirst = (): boolean => {
        return page === 1;
    };

    const render = () => {
        if (isFirst()) {
            return (
              <div className={'pagination'}>
                  <span>{page}</span>
                  <span  className={'pointer'} onClick={() => onChose(page + 1)}>{page + 1}</span>
              </div>
            );
        } else if (isLast()) {
            return (
                <div className={'pagination'}>
                    <span  className={'pointer'} onClick={() => onChose(page - 1)}>{page - 1}</span>
                    <span>{page}</span>
                </div>
            );
        } else {
            return (
                <div className={'pagination'}>
                    <span  className={'pointer'} onClick={() => onChose(page - 1)}>{page - 1}</span>
                    <span>{page}</span>
                    <span  className={'pointer'} onClick={() => onChose(page + 1)}>{page + 1}</span>
                </div>
            );
        }
    };

    return render();
}
