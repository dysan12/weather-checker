import * as React from 'react';
import { GoogleMap as OriginGoogleMap, withGoogleMap, withScriptjs } from 'react-google-maps';
import ReactModal from 'react-modal';
import ReactLoader from "react-loader";
import Fetcher from '../API/Fetcher';

interface GoogleMapProps {
    defaultCenter: google.maps.LatLngLiteral,
    zoom: number,
}

interface GoogleMapState {
    modal: {
        isOpen: boolean,
        isLoading: boolean,
        forecast?: Forecast
    }
}

interface Forecast {
    city: string,
    coords: {
        lat: number,
        lon: number
    }
    cloudiness: string,
    description: string,
    temperature: string,
    windSpeed: string
}

export default class GoogleMap extends React.Component<GoogleMapProps, GoogleMapState> {
    state = {
        modal: {
            isOpen: false,
            isLoading: false,
            forecast: null
        }
    };

    onMapClick = (e: google.maps.MouseEvent) => {
        const position = e.latLng;
        this.setState(prevState => {
            return {
                ...prevState,
                modal: {
                    isOpen: true,
                    isLoading: true,
                    forecast: null
                }
            }
        });
        this.loadForecast(position.lat(), position.lng());
    };

    closeModal = () => {
        this.setState(prevState => {
            return {
                ...prevState,
                modal: {
                    isOpen: false,
                    isLoading: false,
                    forecast: null
                }
            }
        })
    };

    loadForecast = async (lat: number, lon: number) => {
        try {
            const response = await Fetcher.fetchForecast(lat, lon);
            const body = await response.json();

            this.setState(prevState => {
                return {
                    ...prevState,
                    modal: {
                        isOpen: true,
                        isLoading: false,
                        forecast: {
                            city: body.city,
                            coords: {
                                lat: body.coords.lat,
                                lon: body.coords.lon,
                            },
                            cloudiness: body.cloudiness,
                            description: body.description,
                            temperature: body.temperature,
                            windSpeed: body.windSpeed,
                        }
                    }
                }
            });
        } catch (e) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    modal: {
                        isOpen: false,
                        isLoading: false,
                        forecast: null
                    }
                }
            });
        }
    };

    public render() {
        const { modal } = this.state;

        let forecast;
        if (modal.forecast !== null) {
            forecast = (
                <div>
                    <header>
                        <h3>Pogoda dla miasta {modal.forecast.city}</h3>
                    </header>
                    <div>
                        <p><span className={'bold'}>Koordynaty: </span>{`(${modal.forecast.coords.lat} : ${modal.forecast.coords.lon})`}</p>
                        <p><span className={'bold'}>Temperatura: </span>{modal.forecast.temperature}</p>
                        <p><span className={'bold'}>Zachmurzenie: </span>{modal.forecast.cloudiness}</p>
                        <p><span className={'bold'}>Prędkość wiatru: </span>{modal.forecast.windSpeed}</p>
                        <p><span className={'bold'}>Opis: </span>{modal.forecast.description}</p>
                    </div>
                </div>
            )
        } else {
            forecast = (
                <div className={'statistics'}>
                    <div>
                        <span>Nie udało się załadować danych.</span>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <Map
                    {...this.props}
                    onClick={this.onMapClick}
                    googleMapURL={`${process.env.REACT_APP_GOOGLE_MAP_API}`}
                    loadingElement={<div style={{ height: `100%` }}/>}
                    containerElement={<div style={{ height: `700px` }}/>}
                    mapElement={<div style={{ height: `100%` }}/>}
                />
                <ReactModal
                    isOpen={modal.isOpen}
                    onRequestClose={this.closeModal}
                    shouldCloseOnEsc={true}
                    shouldCloseOnOverlayClick={true}
                    className={'forecast-modal'}
                    overlayClassName={'forecast-modal-overlay'}
                >
                    <ReactLoader
                        loaded={!modal.isLoading}
                        className={"GridLoader"}
                    >
                        {forecast}
                    </ReactLoader>
                </ReactModal>
            </div>
        );
    }
}

const Map = withScriptjs(withGoogleMap((props: GoogleMapProps & { onClick: (e: google.maps.MouseEvent) => void }) => {
    return (
        <OriginGoogleMap
            defaultCenter={props.defaultCenter} zoom={props.zoom}
            onClick={props.onClick}
            options={{
                disableDefaultUI: true
            }}
        >
        </OriginGoogleMap>
    );
}));
