class Fetcher {
    public constructor(private apiDomain: string){};

    public async fetchStatistics()  {
        return fetch(`${this.apiDomain}/statistics`);
    }

    public async fetchHistoricalForecasts(limit: number, page: number)  {
        return fetch(`${this.apiDomain}/forecasts?limit=${limit}&page=${page}`);
    }

    public async fetchForecast(lat: number, lon: number)  {
        return fetch(`${this.apiDomain}/forecasts`, {
            method: 'POST',
            body: JSON.stringify({ lat, lon })
        });
    }
}

export default new Fetcher(process.env.REACT_APP_API_DOMAIN);
