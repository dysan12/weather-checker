<?php
declare(strict_types=1);

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

/**
 * Michał Gaj
 * Date: 13.03.19
 * Email: michalgaj@onet.pl
 */
class ForecastCest
{
    public function getForecastForCoords(ApiTester $I): void
    {
        $I->haveHttpHeader('Content-type', 'application/json');
        $I->sendPOST('/forecasts', [
            'lat' => 40,
            'lon' => 40
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.city');
        $I->seeResponseJsonMatchesJsonPath('$.coords');
        $I->seeResponseJsonMatchesJsonPath('$.coords.lat');
        $I->seeResponseJsonMatchesJsonPath('$.coords.lon');
        $I->seeResponseJsonMatchesJsonPath('$.temperature');
        $I->seeResponseJsonMatchesJsonPath('$.cloudiness');
        $I->seeResponseJsonMatchesJsonPath('$.windSpeed');
        $I->seeResponseJsonMatchesJsonPath('$.description');
    }

    public function getForecastWithInvalidCoords(ApiTester $I): void
    {
        $I->haveHttpHeader('Content-type', 'application/json');
        $I->sendPOST('/forecasts', [
            'lat' => 91,
            'lon' => 181
        ]);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }

    public function getForecastsHistory(ApiTester $I): void
    {
        $I->sendGET('/forecasts');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.forecasts');
        $I->seeResponseMatchesJsonType(['forecasts' => 'array']);
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..city');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords.lat');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords.lon');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..temperature');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..cloudiness');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..windSpeed');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..description');
        $I->seeResponseJsonMatchesJsonPath('$._pagination');
        $I->seeResponseMatchesJsonType(['_pagination' => 'array']);
        $I->seeResponseJsonMatchesJsonPath('$._pagination.page');
        $I->seeResponseJsonMatchesJsonPath('$._pagination.limit');
        $I->seeResponseJsonMatchesJsonPath('$._pagination.size');
    }

    public function getForecastsHistoryPaginatedFormat(ApiTester $I): void
    {
        $I->sendGET('/forecasts?pag[perPage]=10&pag[offset]=2');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.forecasts');
        $I->seeResponseMatchesJsonType(['forecasts' => 'array']);
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..city');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords.lat');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..coords.lon');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..temperature');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..cloudiness');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..windSpeed');
        $I->seeResponseJsonMatchesJsonPath('$.forecasts..description');
        $I->seeResponseJsonMatchesJsonPath('$._pagination');
        $I->seeResponseMatchesJsonType(['_pagination' => 'array']);
        $I->seeResponseJsonMatchesJsonPath('$._pagination.page');
        $I->seeResponseJsonMatchesJsonPath('$._pagination.limit');
        $I->seeResponseJsonMatchesJsonPath('$._pagination.size');
    }

    public function getForecastsHistoryWithInvalidPagination(ApiTester $I): void
    {
        $I->sendGET('/forecasts?limit=2&page=-1');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
    }

    public function getStatistics(ApiTester $I): void
    {
        $I->sendGET('/statistics');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseJsonMatchesJsonPath('$.maxTemp');
        $I->seeResponseJsonMatchesJsonPath('$.minTemp');
        $I->seeResponseJsonMatchesJsonPath('$.avgTemp');
        $I->seeResponseJsonMatchesJsonPath('$.searchesQuantity');
        $I->seeResponseJsonMatchesJsonPath('$.mostSearchedCity');
    }
}
