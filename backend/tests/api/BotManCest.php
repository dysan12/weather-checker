<?php
declare(strict_types=1);

use App\Tests\ApiTester;

/**
 * Michał Gaj
 * Date: 13.03.19
 * Email: michalgaj@onet.pl
 */
class BotManCest
{
    public function getBotManChat(ApiTester $I)
    {
        $I->sendGET('/botman/chat');
        $I->seeResponseCodeIs(200);
    }

    public function talkToBotMan(ApiTester $I)
    {
        $I->sendGET('/botman');
        $I->seeResponseCodeIs(200);
    }
}
