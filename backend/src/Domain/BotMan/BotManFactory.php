<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 08.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\BotMan;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\Interfaces\CacheInterface;
use BotMan\BotMan\BotManFactory as SourceFactory;

class BotManFactory
{
    public function create(array $config, string $driverClassName, CacheInterface $cache): BotMan
    {
        DriverManager::loadDriver($driverClassName);

        return SourceFactory::create($config, $cache);
    }
}
