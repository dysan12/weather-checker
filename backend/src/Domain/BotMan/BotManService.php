<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\BotMan;

use App\Domain\BotMan\Conversation\WeatherConversation;
use BotMan\BotMan\BotMan;

class BotManService
{
    /**
     * @var BotMan
     */
    private $bot;

    /**
     * @var WeatherConversation
     */
    private $weatherConversation;

    public function __construct(BotMan $bot, WeatherConversation $weatherConversation)
    {
        $this->bot = $bot;
        $this->weatherConversation = $weatherConversation;
    }

    public function startWeatherConversation(): void
    {
        $this->bot->hears('(cześć|czesc|witaj|dzień dobry|dzien dobry)', function (BotMan $bot) {
            $bot->startConversation($this->weatherConversation);
        });

        $this->bot->listen();
    }
}
