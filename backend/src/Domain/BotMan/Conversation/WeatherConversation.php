<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 07.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\BotMan\Conversation;

use App\Domain\Forecast\Entity\Forecast;
use App\Domain\Forecast\Exception\UnableToCheckWeather;
use App\Domain\Forecast\ForecastService;
use App\Kernel;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class WeatherConversation extends Conversation implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    private $translator;

    public function __construct(ContainerInterface $container, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->translator = $translator;
    }

    public function greet(): void
    {
//        $this->say($this->translator->trans('Cześć [Imię]!.'));
        $this->askCity();
    }

    public function askCity(): void
    {
        $this->ask(
            $this->translator->trans('Dla jakiego miasta chcesz sprawdzić aktualną pogodę?'),
            function (Answer $answer) {
                $this->checkWeather($answer->getText());
        });
    }

    public function checkWeather(string $city): void
    {
        try {
            /** @var Forecast $forecast */
            $forecast = $this->container->get(ForecastService::class)->checkWeatherByCityName($city);
            $msg = '';
            $msg .= $this->translator->trans('Pogoda dla miasta') . ' ' . ucfirst(strtolower($city)) . ':<br>';
            $msg .= $this->translator->trans('Temperatura') . ': ' . $forecast->getTemp() . '&deg;C<br>';
            $msg .= $this->translator->trans('Zachmurzenie') . ': ' . $forecast->getCloudiness() . '%<br>';
            $msg .= $this->translator->trans('Prędkość wiatru') . ': ' . $forecast->getWindSpeed() . 'm/s<br>';
            $msg .= $this->translator->trans('Opis') . ': ' . $forecast->getDescription() . '<br>';

            $this->say($msg);

        } catch (UnableToCheckWeather $e) {
            $this->say(
                sprintf(
                    $this->translator->trans('Przepraszam, nie potrafię sprawdzić pogody dla miasta') . ': "%s".'
                    , $city)
            );
        }

        $this->askContinue();
    }

    public function askContinue(): void
    {
        $question = new Question($this->translator->trans('Czy chcesz sprawdzić pogodę dla innego miasta?'));
        $question->addButtons([
            Button::create($this->translator->trans('Tak'))->value(1),
            Button::create($this->translator->trans('Nie'))->value(0),
        ]);

        $this->ask($question, function (Answer $answer) {
            if ($answer->getValue()) {
                $this->askCity();
            } else {
                $this->farewell();
            }
        });
    }

    public function farewell(): void
    {
        $this->say($this->translator->trans('W takim razie do zobaczenia!'));
    }

    public function run()
    {
        $this->greet();
    }

    /**
     * Sets the container.
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function __wakeup()
    {
        $env = $_SERVER['APP_ENV'] ?? 'dev';
        $debug = (bool) ($_SERVER['APP_DEBUG'] ?? ('prod' !== $env));
        $kernel = new Kernel($env, $debug);
        $kernel->boot();
        $this->setContainer($kernel->getContainer());
        $this->translator = $this->container->get('symfony.translator');
    }
}
