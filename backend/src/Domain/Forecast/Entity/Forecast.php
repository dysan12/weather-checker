<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Forecast
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $lat;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $lon;

    /**
     * @var string
     * @ORM\Column()
     */
    private $cityName;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $temp;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $cloudiness;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $windSpeed;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $windDegree;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @param float $lon
     */
    public function setLon(float $lon): void
    {
        $this->lon = $lon;
    }

    /**
     * @param string $cityName
     */
    public function setCityName(string $cityName): void
    {
        $this->cityName = $cityName;
    }

    /**
     * @param float $temp
     */
    public function setTemp(float $temp): void
    {
        $this->temp = $temp;
    }

    /**
     * @param float $cloudiness
     */
    public function setCloudiness(float $cloudiness): void
    {
        $this->cloudiness = $cloudiness;
    }

    /**
     * @param float $windSpeed
     */
    public function setWindSpeed(float $windSpeed): void
    {
        $this->windSpeed = $windSpeed;
    }

    /**
     * @param float $windDegree
     */
    public function setWindDegree(float $windDegree): void
    {
        $this->windDegree = $windDegree;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return float
     */
    public function getTemp(): float
    {
        return $this->temp;
    }

    /**
     * @return float
     */
    public function getCloudiness(): float
    {
        return $this->cloudiness;
    }

    /**
     * @return float
     */
    public function getWindSpeed(): float
    {
        return $this->windSpeed;
    }

    /**
     * @return float
     */
    public function getWindDegree(): float
    {
        return $this->windDegree;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
