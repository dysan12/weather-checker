<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast;

use App\Domain\Forecast\Entity\Forecast;
use App\Domain\Forecast\Exception\UnableToCheckWeather;
use App\Domain\Forecast\WeatherProvider\WeatherProviderInterface;

class ForecastService
{
    /**
     * @var WeatherProviderInterface
     */
    private $weatherProvider;

    /**
     * @var ForecastPersistenceInterface
     */
    private $forecastPersistence;

    public function __construct(WeatherProviderInterface $weatherProvider, ForecastPersistenceInterface $forecastPersistence)
    {
        $this->weatherProvider = $weatherProvider;
        $this->forecastPersistence = $forecastPersistence;
    }

    /**
     * @param CoordsInterface $coords
     * @return Forecast
     * @throws UnableToCheckWeather
     */
    public function checkWeatherByCoords(CoordsInterface $coords): Forecast
    {
        $forecast = $this->weatherProvider->checkWeatherByCoords($coords);
        if (null === $forecast) {
            throw new UnableToCheckWeather(sprintf('Unable to check weather for coords: %s:%s', $coords->getLon()->getLon(), $coords->getLat()->getLat()));
        }

        $this->forecastPersistence->persist($forecast);
        $this->forecastPersistence->flush();

        return $forecast;
    }

    /**
     * @param string $cityName
     * @return Forecast
     * @throws UnableToCheckWeather
     */
    public function checkWeatherByCityName(string $cityName): Forecast
    {
        $forecast = $this->weatherProvider->checkWeatherByCityName($cityName);
        if (null === $forecast) {
            throw new UnableToCheckWeather(sprintf('Unable to check weather for city: %s', $cityName));
        }

        $this->forecastPersistence->persist($forecast);
        $this->forecastPersistence->flush();

        return $forecast;
    }
}
