<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast;

use App\Domain\Forecast\VO\Latitude;
use App\Domain\Forecast\VO\Longitude;

interface CoordsInterface
{
    public function getLat(): Latitude;
    public function getLon(): Longitude;
}
