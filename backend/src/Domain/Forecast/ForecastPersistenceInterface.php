<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 12.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast;

use App\Domain\Forecast\Entity\Forecast;

interface ForecastPersistenceInterface
{
    public function persist(Forecast $forecast): void;
    public function flush(): void;
}
