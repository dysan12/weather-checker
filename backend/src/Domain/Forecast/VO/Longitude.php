<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\VO;

class Longitude
{
    /**
     * @var float
     */
    private $lon;

    public function __construct(float $lon)
    {
        if ($lon > 180 || $lon < -180) {
            throw new \InvalidArgumentException('Longitude must be in range <-180;180>');
        }
        $this->lon = $lon;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }
}
