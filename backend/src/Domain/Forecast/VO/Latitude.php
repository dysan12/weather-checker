<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\VO;

class Latitude
{
    /**
     * @var float
     */
    private $lat;

    public function __construct(float $lat)
    {
        if ($lat > 90 || $lat < -90) {
            throw new \InvalidArgumentException('Longitude must be in range <-90;90>');
        }
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }
}
