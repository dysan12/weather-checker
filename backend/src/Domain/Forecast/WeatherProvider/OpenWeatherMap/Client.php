<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\WeatherProvider\OpenWeatherMap;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var array
     */
    private $config;

    public function __construct(string $apiUrl, string $appID, ?array $config)
    {
        $this->apiUrl = $apiUrl;
        $this->client = new \GuzzleHttp\Client([
            RequestOptions::HTTP_ERRORS => false
        ]);
        $this->config = array_merge($config ?? [], ['APPID' => $appID]);
    }

    public function get(string $uri, array $parameters, array $options = []): ResponseInterface
    {
        $uri .= '?' . $this->convertParametersToString($parameters);

        return $this->client->request('GET',$this->apiUrl . $uri, $options);

    }

    private function convertParametersToString(array $params = []): string
    {
        $parameters = array_merge($this->config, $params);

        $asParams = [];
        foreach ($parameters as $key => $value) {
            $asParams[] = $key . '=' . $value;
        }

        return implode('&', $asParams);
    }
}
