<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\WeatherProvider\OpenWeatherMap;

use App\Domain\Forecast\CoordsInterface;
use App\Domain\Forecast\Entity\Forecast;
use App\Domain\Forecast\WeatherProvider\WeatherProviderInterface;

class OpenWeatherMap implements WeatherProviderInterface
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function checkWeatherByCoords(CoordsInterface $coords): ?Forecast
    {
        $response = $this->client->get('/weather', ['lat' => $coords->getLat()->getLat(), 'lon' => $coords->getLon()->getLon()]);

        $forecast = null;
        if (200 === $response->getStatusCode()) {
            $body = json_decode($response->getBody()->getContents(), true);

            $forecast = new Forecast();
            $forecast->setCityName($body['name'] ?? '');
            $forecast->setCloudiness($body['clouds']['all'] ?? 0);
            $forecast->setDescription($body['weather'][0]['description'] ?? '');
            $forecast->setLat($body['coord']['lat'] ?? 0);
            $forecast->setLon($body['coord']['lon'] ?? 0);
            $forecast->setTemp($body['main']['temp'] ?? 0);
            $forecast->setWindDegree($body['wind']['deg'] ?? 0);
            $forecast->setWindSpeed($body['wind']['speed'] ?? 0);
        }

        return $forecast;
    }

    public function checkWeatherByCityName(string $cityName): ?Forecast
    {
        $response = $this->client->get('/weather', ['q' => $cityName]);

        $forecast = null;
        if (200 === $response->getStatusCode()) {
            $body = json_decode($response->getBody()->getContents(), true);

            $forecast = new Forecast();
            $forecast->setCityName($body['name'] ?? '');
            $forecast->setCloudiness($body['clouds']['all'] ?? 0);
            $forecast->setDescription($body['weather'][0]['description'] ?? '');
            $forecast->setLat($body['coord']['lat'] ?? 0);
            $forecast->setLon($body['coord']['lon'] ?? 0);
            $forecast->setTemp($body['main']['temp'] ?? 0);
            $forecast->setWindDegree($body['wind']['deg'] ?? 0);
            $forecast->setWindSpeed($body['wind']['speed'] ?? 0);
        }

        return $forecast;
    }
}
