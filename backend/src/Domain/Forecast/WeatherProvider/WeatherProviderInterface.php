<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Domain\Forecast\WeatherProvider;

use App\Domain\Forecast\CoordsInterface;
use App\Domain\Forecast\Entity\Forecast;

interface WeatherProviderInterface
{
    public function checkWeatherByCoords(CoordsInterface $coords): ?Forecast;
    public function checkWeatherByCityName(string $cityName): ?Forecast;
}
