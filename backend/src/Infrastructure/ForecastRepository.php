<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\Infrastructure;

use App\Domain\Forecast\Entity\Forecast;
use App\Domain\Forecast\ForecastPersistenceInterface;
use App\Domain\Forecast\ForecastRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ForecastRepository extends ServiceEntityRepository implements ForecastRepositoryInterface, ForecastPersistenceInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Forecast::class);
    }

    public function getQueryBuilder(string $alias): QueryBuilder
    {
        return $this->createQueryBuilder($alias);
    }

    public function getMaxTemp()
    {
        return $this->createQueryBuilder('f')
            ->select('MAX(f.temp)');
    }

    public function getMostSearchedCity()
    {
        return $this->createQueryBuilder('f')
            ->select('f.cityName, count(f.cityName) as searches')
            ->groupBy('f.cityName')
            ->orderBy('searches', 'desc')
            ->setMaxResults(1);
    }

    public function persist(Forecast $forecast): void
    {
        $this->_em->persist($forecast);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }
}
