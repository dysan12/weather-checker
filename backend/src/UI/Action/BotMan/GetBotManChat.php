<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\BotMan;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UI\Responder;


/**
 * @Route(path="/botman/chat", methods={"GET", "POST"})
 */
class GetBotManChat
{
    /**
     * @var Responder\BotMan
     */
    private $botManResponder;

    public function __construct(Responder\BotMan $botManResponder)
    {
        $this->botManResponder = $botManResponder;
    }

    public function __invoke(): Response
    {
        return $this->botManResponder->responseChat();
    }
}
