<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\BotMan;

use App\Domain\BotMan\BotManService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UI\Responder;

/**
 * @Route(path="/botman", methods={"POST", "GET"})
 */
class TalkToBotMan
{
    /**
     * @var Responder\BotMan
     */
    private $botManResponder;

    /**
     * @var BotManService
     */
    private $botManService;

    public function __construct(BotManService $botManService, Responder\BotMan $botManResponder)
    {
        $this->botManResponder = $botManResponder;
        $this->botManService = $botManService;
    }

    public function __invoke(): Response
    {
        $this->botManService->startWeatherConversation();

        return $this->botManResponder->responseTalk();
    }
}
