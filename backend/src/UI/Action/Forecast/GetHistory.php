<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\Forecast;

use App\Domain\Forecast\ForecastRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UI\Responder;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route(path="/forecasts", methods={"GET"})
 */
class GetHistory
{
    /**
     * @var Responder\Forecasts
     */
    private $forecastsResponder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Responder\Error
     */
    private $errorResponder;

    /**
     * @var ForecastRepositoryInterface
     */
    private $forecastRepository;

    public function __construct(
        Responder\Forecasts $forecastsResponder,
        ValidatorInterface $validator,
        Responder\Error $errorResponder,
        ForecastRepositoryInterface $forecastRepository
    )
    {
        $this->forecastsResponder = $forecastsResponder;
        $this->validator = $validator;
        $this->errorResponder = $errorResponder;
        $this->forecastRepository = $forecastRepository;
    }

    public function __invoke(Request $request): Response
    {
        $page = (int)($request->get('page') ?? 1);
        $limit = (int)($request->get('limit') ?? 10);

        $pagination = new Responder\Common\Pagination($page, $limit);
        $errors = $this->validator->validate($pagination);
        if (count($errors)) {
            return $this->errorResponder->responseBadRequestFromViolations($errors);
        }

        $query = $this->forecastRepository->getQueryBuilder('f')
            ->orderBy('f.createdAt', 'desc')
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $paginator = new Paginator($query);
        $pagination->setCount(count($paginator));

        $forecasts = [];
        foreach ($paginator as $forecast) {
            $forecasts[] = $forecast;
        }

        return $this->forecastsResponder->responseForecasts($forecasts, $pagination);
    }
}
