<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\Forecast;

use App\Domain\Forecast\Exception\UnableToCheckWeather;
use App\Domain\Forecast\ForecastService;
use App\UI\Request\Coords;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UI\Responder;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route(path="/forecasts", methods={"POST"})
 */
class CheckForecast
{
    /**
     * @var Responder\Forecasts
     */
    private $forecastsResponder;

    /**
     * @var ForecastService
     */
    private $forecastService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Responder\Error
     */
    private $errorResponder;

    public function __construct(
        Responder\Forecasts $forecastsResponder,
        ForecastService $forecastService,
        ValidatorInterface $validator,
        Responder\Error $errorResponder
    )
    {
        $this->forecastsResponder = $forecastsResponder;
        $this->forecastService = $forecastService;
        $this->validator = $validator;
        $this->errorResponder = $errorResponder;
    }

    public function __invoke(Request $request): Response
    {
        $body = json_decode($request->getContent(), true) ?? [];
        $coords = new Coords($body['lat'] ?? null, $body['lon'] ?? null);

        $errors = $this->validator->validate($coords);
        if (count($errors)) {
            return $this->errorResponder->responseBadRequestFromViolations($errors);
        }

        try {
            $forecast = $this->forecastService->checkWeatherByCoords($coords);
        } catch (UnableToCheckWeather $e) {
            return $this->errorResponder->responseInternalError();
        }

        return $this->forecastsResponder->responseForecast($forecast);
    }
}
