<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Action\Forecast;

use App\Domain\Forecast\ForecastRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\UI\Responder;

/**
 * @Route(path="/statistics", methods={"GET"})
 */
class GetStatistics
{

    /**
     * @var ForecastRepositoryInterface
     */
    private $forecastRepository;

    /**
     * @var Responder\Statistics
     */
    private $statisticsResponder;

    public function __construct(
        Responder\Statistics $statisticsResponder,
        ForecastRepositoryInterface $forecastRepository
    ) {
        $this->forecastRepository = $forecastRepository;
        $this->statisticsResponder = $statisticsResponder;
    }

    public function __invoke(Request $request): Response
    {
        $maxTemp = array_pop($this->forecastRepository->getQueryBuilder('f')
            ->select('MAX(f.temp)')->getQuery()->getResult()[0]);
        $minTemp = array_pop($this->forecastRepository->getQueryBuilder('f')
            ->select('MIN(f.temp)')->getQuery()->getResult()[0]);
        $avgTemp = array_pop($this->forecastRepository->getQueryBuilder('f')
            ->select('AVG(f.temp)')->getQuery()->getResult()[0]);
        $searchesQuantity = array_pop($this->forecastRepository->getQueryBuilder('f')
            ->select('count(f.id)')->getQuery()->getResult()[0]);
        $mostSearchedCity = array_pop($this->forecastRepository->getQueryBuilder('f')
            ->select('f.cityName')
            ->groupBy('f.cityName')
            ->orderBy('count(f.cityName)', 'desc')
            ->setMaxResults(1)->getQuery()->getResult()[0]);

        return $this->statisticsResponder->responseStatistics((float)$maxTemp, (float)$minTemp, (float)$avgTemp,
            (int)$searchesQuantity,
            $mostSearchedCity);
    }
}
