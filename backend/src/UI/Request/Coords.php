<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Request;

use App\Domain\Forecast\CoordsInterface;
use App\Domain\Forecast\VO\Latitude;
use App\Domain\Forecast\VO\Longitude;
use Symfony\Component\Validator\Constraints as Assert;

class Coords implements CoordsInterface
{
    /**
     * @var float
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(-90)
     * @Assert\LessThanOrEqual(90)
     * @Assert\Type(type="numeric")
     */
    private $lat;

    /**
     * @var float
     * @Assert\NotNull()
     * @Assert\GreaterThanOrEqual(-180)
     * @Assert\LessThanOrEqual(180)
     * @Assert\Type(type="numeric")
     */
    private $lon;

    public function __construct($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    public function getLat(): Latitude
    {
        return new Latitude((float)$this->lat);
    }

    public function getLon(): Longitude
    {
        return new Longitude((float)$this->lon);
    }
}
