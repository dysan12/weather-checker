<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 14.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Responder\Common;

use Symfony\Component\Validator\Constraints as Assert;

class Pagination
{
    /**
     * @var int
     * @Assert\GreaterThan(0)
     * @Assert\Type(type="numeric")
     */
    private $page;

    /**
     * @var int
     * @Assert\GreaterThan(0)
     * @Assert\Type(type="numeric")
     */
    private $limit;

    /**
     * @var int
     */
    private $count = 0;

    public function __construct(int $page, int $limit)
    {
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}
