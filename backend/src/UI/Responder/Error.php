<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Responder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class Error
{
    public function responseBadRequest(array $msgs): Response
    {
        return new JsonResponse(['messages' => $msgs], 400);
    }

    public function responseBadRequestFromViolations(ConstraintViolationListInterface $errors): Response
    {
        $msgs = [];
        /** @var ConstraintViolationInterface $error */
        foreach ($errors as $error) {
            $msgs[$error->getPropertyPath()][] = $error->getMessage();
        }

        return new JsonResponse(['messages' => $msgs], 400);
    }

    public function responseInternalError(): Response
    {
        return new JsonResponse(null, 500);
    }
}
