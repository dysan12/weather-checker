<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Responder;

use Symfony\Component\HttpFoundation\Response;

class BotMan
{
    /**
     * @var \Twig\Environment
     */
    private $twig;

    public function __construct(\Twig\Environment $twig)
    {
        $this->twig = $twig;
    }

    public function responseChat(): Response
    {
        $content = $this->twig->render('botman.html.twig');

        return new Response($content);
    }

    public function responseTalk(): Response
    {
        return new Response();
    }
}
