<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Responder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Statistics
{
    public function responseStatistics(
        float $maxTemp,
        float $minTemp,
        float $avgTemp,
        int $searchesQuantity,
        string $mostSearchedCity
    ): Response {
        return new JsonResponse([
            'maxTemp' => $maxTemp,
            'minTemp' => $minTemp,
            'avgTemp' => $avgTemp,
            'searchesQuantity' => $searchesQuantity,
            'mostSearchedCity' => $mostSearchedCity,
        ]);
    }
}
