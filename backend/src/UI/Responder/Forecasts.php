<?php
declare(strict_types=1);
/**
 * Michał Gaj
 * Date: 11.03.19
 * Email: michalgaj@onet.pl
 */

namespace App\UI\Responder;

use App\Domain\Forecast\Entity\Forecast;
use App\UI\Responder\Common\Pagination;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Forecasts
{
    public function responseForecast(Forecast $forecast): JsonResponse
    {
        return new JsonResponse($this->getSerializedForecast($forecast));
    }

    public function responseForecasts(array $forecasts, Pagination $pagination): Response
    {
        $serializedForecasts = [];
        foreach ($forecasts as $forecast) {
            if ($forecast instanceof Forecast) {
                $serializedForecasts[] = $this->getSerializedForecast($forecast);
            }
        }

        return new JsonResponse([
            'forecasts' => $serializedForecasts,
            '_pagination' => [
                'page' => $pagination->getPage(),
                'limit' => $pagination->getLimit(),
                'size' => $pagination->getCount()
            ]
        ]);
    }

    private function getSerializedForecast(Forecast $forecast): array
    {
        return [
            'city' => $forecast->getCityName(),
            'coords' => [
              'lat' => $forecast->getLat(),
              'lon' => $forecast->getLon()
            ],
            'temperature' => $forecast->getTemp() . ' stopni Celsjusza',
            'cloudiness' => $forecast->getCloudiness() . '%',
            'windSpeed' => $forecast->getWindSpeed() . 'm/s',
            'description' => $forecast->getDescription(),
        ];
    }
}
